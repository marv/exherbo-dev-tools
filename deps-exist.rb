#!/usr/bin/env ruby
# vim: set sw=4 sts=4 et tw=80 :

# Copyright 2014 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# This is a script to, given a list of packages, verify that all potential
# dependencies of those packages exist (even if only in an unwritten repo)

require 'Paludis'

# Take a list of dep spec strings, parse them, look up
# the matching package IDs, and flatten the result into
# a one-level list
def expand_user_targets( environment, targets )
    targets.lazy.flat_map { |target_str|
        environment[
            # Use ...GroupedBySlot so the output is in the most
            # useful ordering in the case of wildcards and such
            Paludis::Selection::AllVersionsGroupedBySlot.new(
                Paludis::FilteredGenerator.new(
                    Paludis::Generator::Matches.new(
                        Paludis.parse_user_package_dep_spec(
                            target_str,
                            environment,
                            [ :allow_wildcards, :no_disambiguation ]
                        ),
                        nil,
                        []
                    ),
                    Paludis::Filter::SupportsAction.new( Paludis::InstallAction )
                )
            )
        ]
    }
end

def flatten_dep_spec( spec )
    case spec
    when Paludis::AllDepSpec
        spec.flat_map { |x|
            flatten_dep_spec( x )
        }
    when Paludis::AnyDepSpec
        spec.flat_map { |x|
            flatten_dep_spec( x )
        }
    when Paludis::AtMostOneDepSpec
        spec.flat_map { |x|
            flatten_dep_spec( x )
        }
    when Paludis::BlockDepSpec
        # Blocks on nonexistent packages always succeed, and may
        # be needed due to lingering installed packages
    when Paludis::ConditionalDepSpec
        # Ignore the condition
        spec.flat_map { |x|
            flatten_dep_spec( x )
        }
    when Paludis::DependenciesLabelsDepSpec
    when Paludis::ExactlyOneDepSpec
        spec.flat_map { |x|
            flatten_dep_spec( x )
        }
    when Paludis::PackageDepSpec
        [ spec ]
    else
        puts spec.class
        exit 1
    end
end

def flatten_dependencies_key( key )
    if key
        flatten_dep_spec( key.parse_value ).select { |x| x }
    else
        # Nil
        []
    end
end

def all_potential_dependencies( target )
    [
        flatten_dependencies_key( target.build_dependencies_key ),
        flatten_dependencies_key( target.dependencies_key ),
        flatten_dependencies_key( target.post_dependencies_key ),
        flatten_dependencies_key( target.run_dependencies_key ),
    ].flatten()
end

def dependencies_for_targets( env, targets )
    expand_user_targets( env, ARGV ).map { |target|
        {
            id: target,
            deps: all_potential_dependencies( target ),
        }
    }
end

def dependency_missing( env, pkg, dep )
    env[
        Paludis::Selection::SomeArbitraryVersion.new(
            Paludis::Generator::Matches.new(
                dep,
                pkg,
                []
            )
        )
    ].empty?
end

Paludis::Log.instance.log_level = Paludis::LogLevel::Warning
Paludis::Log.instance.program_name = $0

env = Paludis::PaludisEnvironment.new

puts "These dependencies match nothing; please at least add them to ::unwritten:"
puts
has_missing_deps = dependencies_for_targets( env, ARGV ).map { |pkg|
    {
        id: pkg[:id],
        deps: pkg[:deps].select { |dep|
            dependency_missing( env, pkg[:id], dep )
        },
    }
}.select { |pkg|
    not pkg[:deps].empty?
}.map { |pkg|
    puts "From #{pkg[:id]}"
    pkg[:deps].each { |dep|
        puts "    #{dep}"
    }
    puts

    pkg
}

if has_missing_deps.count == 0
    exit 0
else
    exit 1
end
